'use strict';

describe('Service: FURL', function () {

  // load the service's module
  beforeEach(module('artkickerApp'));

  // instantiate service
  var FURL;
  beforeEach(inject(function (_FURL_) {
    FURL = _FURL_;
  }));

  it('should do something', function () {
    expect(!!FURL).toBe(true);
  });

});
