'use strict';

/**
 * @ngdoc function
 * @name artkickerApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the artkickerApp
 */
angular.module('artkickerApp')
  .controller('MainCtrl', function ($scope, $firebaseArray, FURL, Auth) {
    var teamsRef = new Firebase(FURL + '/teams'),
      usersRef = new Firebase(FURL + '/profile'),
      currentUser = Auth.user;

    $scope.teams = $firebaseArray(teamsRef);
    $scope.users = $firebaseArray(usersRef);

    $scope.addTeam = function () {
      $scope.teams.$add({
        title: $scope.title,
        captain: currentUser.uid,
        player: '',
        games: 0,
        wins: 0,
        looses: 0
      });

      $scope.title = '';
    };
  });
