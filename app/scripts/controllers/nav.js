'use strict';

/**
 * @ngdoc function
 * @name artkickerApp.controller:NavCtrl
 * @description
 * # NavCtrl
 * Controller of the artkickerApp
 */
angular.module('artkickerApp')
  .controller('NavCtrl', function ($scope, $location, Auth) {

    $scope.currentUser = Auth.user;
    $scope.signedIn = Auth.signedIn;

    $scope.logout = function () {
      Auth.logout();
      $location.path('/');
    };
  });
