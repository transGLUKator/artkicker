'use strict';

/**
 * @ngdoc function
 * @name artkickerApp.controller:AuthCtrl
 * @description
 * # AuthCtrl
 * Controller of the artkickerApp
 */
angular.module('artkickerApp')
    .controller('AuthCtrl', function ($scope, $location, Auth) {
        if (Auth.user.provider) {
            $location.path('/');
        }

        $scope.register = function (user) {
            Auth.register(user).then(function () {
                $location.path('/');
            }, function (err) {
                console.log('Auth went wrong ' + err);
            });
        };

        $scope.login = function (user) {
            Auth.login(user).then(function () {
                $location.path('/');
            }, function (err) {
                console.log(err);
            });
        };


    });
