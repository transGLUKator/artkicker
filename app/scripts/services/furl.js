'use strict';

/**
 * @ngdoc service
 * @name artkickerApp.FURL
 * @description
 * # FURL
 * Constant in the artkickerApp.
 */
angular.module('artkickerApp')
  .constant('FURL', 'https://art-kicker.firebaseio.com');
